﻿using System;

namespace Org.InfinityKey.Library.Monads
{
    /// <summary>
    /// 
    /// </summary>
    /// <summary>
    /// Functional programming "steps".  Allows for the ability to verify nested properties and other inline abilities.
    /// </summary>
    public static class Steps
    {
        /// <summary>
        /// Checks to see if the input is valid.  If it is it will return the input, otherwise returns the default TR value. Useful for short circuiting validations.
        /// </summary>
        /// <typeparam name="T">Class object</typeparam>
        /// <typeparam name="TR">Value item.</typeparam>
        /// <param name="value">The value</param>
        /// <param name="lambdaEval">The lambda evaluation.</param>
        /// <returns><seealso cref="lambdaEval"/> if valid, else returns default(<seealso cref="lambdaVal"/>)</returns>
        public static TR MaybeValue<T, TR>(this T value, Func<T, TR> lambdaEval)
            where T : class
            where TR : struct
        {
            return value == null ? default(TR) : lambdaEval(value);
        }
        /// <summary>
        /// Checks to see if the input is valid.  If it is it will return the input, otherwise returns null.  Useful for short circuiting validations.
        /// </summary>
        /// <typeparam name="T">Class object</typeparam>
        /// <typeparam name="TR">Value item.</typeparam>
        /// <param name="value">The value</param>
        /// <param name="lambdaEval">The lambda evaluation.</param>
        /// <returns><seealso cref="lambdaEval"/> if valid, else returns default(<seealso cref="lambdaVal"/>)</returns>
        public static TR Maybe<T, TR>(this T clazz, Func<T, TR> lambdaEval)
            where T : class
            where TR : class
        {
            return clazz == null ? null : lambdaEval(clazz);
        }
        /// <summary>
        /// Returns the specified class.
        /// </summary>
        /// <typeparam name="T">Class object</typeparam>
        /// <typeparam name="TR">Value item.</typeparam>
        /// <param name="clazz">The class.</param>
        /// <param name="lambdaEval">The lambda evaluation.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns><seealso cref="lambdaEval"/> if valid, else returns null</returns>
        public static TR Return<T, TR>(this T clazz, Func<T, TR> lambdaEval, TR defaultValue) where T : class
        {
            return clazz == null ? defaultValue : lambdaEval(clazz);
        }
        /// <summary>
        /// Assigns the specified class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="clazz">The class.</param>
        /// <param name="assignment">The assignment expression.</param>
        public static void Assign<T>(this T clazz, Action<T> assignment) where T : class
        {
            if (typeof(T).IsInterface || clazz == null) return;

            assignment(clazz);
        }
    }
}
