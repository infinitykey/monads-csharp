﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Org.InfinityKey.Library.Monads
{
    public class Program
    {
        public Program()
        {
            try
            {
                var name = new Country().State.City.Name;
		Console.WriteLine(name); 
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

	    Console.WriteLine();

            try
            {
                var name = new Country().Maybe(x => x.State).Maybe(x => x.City).Return(x => x.Name, "Default");
                Console.WriteLine(name);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

	    Console.WriteLine();

            try
            {
                var name = new Country().Maybe(x => x.State).Maybe(x => x.City).Maybe(x => x.Name);
                Console.WriteLine(name);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

	    Console.WriteLine();

            try
            {
                var name = new Country().Return(x => x.State, new State()).Return(x => x.City,new City()).Maybe(x => x.Name);
                Console.WriteLine(name);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            Console.ReadLine();
        }
        static void Main(string[] args)
        {
            new Program();
        }
    }

    internal class Country
    {
        public State State { get; set; }
    }

    internal class State {
        public City City { get; set; }
    }

    internal class City {
        public City()
        {
            Name = "Zoola";
        }
        public string Name { get; set; }
    }
}
